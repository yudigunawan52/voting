# Voting

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.24.

## Penggunaan App
sebelum kita memulai development server pertama tama kita harus run `npm install` baru setelah itu kita bisa run `ng serve`
bukalah browser dan jalankan url `http://localhost:4200/` 

*  note : ( jika ingin mengunakan backend local maka harus merubah service `rest-api` pada folder service dan pada function link_url tukar dengan `http://localhost:8081`)
*  pertama tama akan membawa kita ke halaman login tapi sebelum itu 
*  kita bisa register terlebih dahulu dengan cara klik tulisan `sign up` di bagian paling bawah card login tersebut
*  isilah data-data kita dan klik tombol `Register` jika gagal akan ada muncul dialog yang mengingatkan kita bahwa kita salah 
   dalam mengisi form tersebut dan jika berhasil maka akan di arahkan ke halaman login 
*  dan kita bisa login dengan email dan password yang telah kita daftarkan sebelumnya jika gagal akan ada juga sebuah dialog peringatan dan jika berhasil akan 
   diarahkan kehalaman home 
*  di halaman home ini terdapat data semua user kecuali user yang login tadi dan terdapat tombol bertulisan `Vote` agar dapat memvote user tersebut
   jika kita mengklik tombol `Vote` tersebut maka akan ada dialog yang memperingati kita ,apakah kita yakin dengan pilihan kita karna jika sudah menekan tombol
   `Yes` yang ada pada dialog tersebut maka kita tidak dapat lagi menvote user yang ada di daftar tersebut
*  kita dapat melihat data orang yang di Vote dengan cara mengakses halaman vote dengan menklik Vote di navigation yang terdapat dibagian paling atas
*  disana terdapat tombol `View` agar kita dapat melihat siapa saja yang telah melakukan vote tersebut

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


