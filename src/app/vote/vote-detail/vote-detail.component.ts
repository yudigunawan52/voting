import { Component, OnInit } from '@angular/core';
import { RestApiService } from 'src/app/service/rest-api.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-vote-detail',
  templateUrl: './vote-detail.component.html',
  styleUrls: ['./vote-detail.component.css']
})
export class VoteDetailComponent implements OnInit {
  dataDetail;
  id;
  constructor(
    private activatedRoute: ActivatedRoute,
    private rest: RestApiService) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(res => {
      this.id = res['id'];
      this.rest.getDetailVote(this.id).subscribe((data) => {
        this.dataDetail = data['detail'];
        console.log(this.dataDetail);
        
      })
    });
  }

}
