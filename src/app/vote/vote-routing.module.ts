import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VoteComponent } from './vote.component';
import { VoteDetailComponent } from './vote-detail/vote-detail.component';

const routes: Routes = [
    {
            path: '',
            component: VoteComponent
        },
        {
            path: 'detail/:id',
            component: VoteDetailComponent
        }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class VoteRoutingModule { }
