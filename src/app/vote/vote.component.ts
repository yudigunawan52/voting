import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatDialog, MatPaginator } from '@angular/material';
import { RestApiService } from '../service/rest-api.service';
import { DataService } from '../service/data.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-vote',
  templateUrl: './vote.component.html',
  styleUrls: ['./vote.component.css']
})
export class VoteComponent implements OnInit {
  displayedColumns: string[] = ['Nama', 'Email','Jumlah', 'Action'];
  dataVote: any = [];
  url = this.rest.link_url();
  totData;
  profile;
  currentUser;
  votingUser;
  count;
  dataSource = new MatTableDataSource();
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(public dialog: MatDialog,
    private http: HttpClient,
    private rest: RestApiService,
    private datas: DataService) {
    datas.getProfile();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  async ngOnInit() {
    await this.rest.getAllVote().subscribe((data) => {
      this.dataVote = data['vote'].map(data=>{
        // data.count = data.count;
        data['_id'][0]['count'] = data['count'];
        // console.log(data);
        data = data['_id'][0];
        // data.count = data.count;
        return data
      });
      // this.count = data['count'];
      console.log(this.dataVote);
      
      this.totData = this.dataVote.length;
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = this.dataVote;
      this.dataSource.paginator = this.paginator;
    })

  }

  vote(arr) {
    
  }

}
