import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VoteComponent } from './vote.component';
import { VoteDetailComponent } from './vote-detail/vote-detail.component';
import { VoteRoutingModule } from "./vote-routing.module";
import { MaterialAppModule } from '../ngmaterial.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ScrollingModule } from '@angular/cdk/scrolling';


@NgModule({
  declarations: [VoteComponent, VoteDetailComponent],
  imports: [
    CommonModule,
    VoteRoutingModule,
    MaterialAppModule,
    FormsModule,
    ScrollingModule,
    ReactiveFormsModule]
})
export class VoteModule { }
