import { NgModule } from '@angular/core';
import {
	MatIconModule,
	MatInputModule,
	MatFormFieldModule,
	MatDialogModule,
	MatPaginatorModule,
	MatCardModule,
	MatTableModule,
	MatListModule,
	MatButtonModule
			} from '@angular/material';

const ExportedMatModules = [
			MatIconModule,
			MatInputModule,
			MatFormFieldModule,
			MatDialogModule,
			MatPaginatorModule,
			MatCardModule,
			MatTableModule,
			MatListModule,
			MatButtonModule
]


@NgModule({
  	imports: [...ExportedMatModules	],
  	exports: [...ExportedMatModules ]
})
export class MaterialAppModule { }
