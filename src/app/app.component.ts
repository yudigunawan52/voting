import { Component } from '@angular/core';
import { DataService } from './service/data.service';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,private data:DataService){
    data.getProfile();
    this.matIconRegistry.addSvgIcon(
      "icon-close",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icon/svg/icon-close.svg")
    );
  }

  Logout() {
    localStorage.clear();
    window.location.replace('/auth/login');
  }
}
