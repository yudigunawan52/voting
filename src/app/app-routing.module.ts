import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from './service/auth-guard.service';

const routes: Routes = [

  {
    // path : '',
    path: 'auth',
    loadChildren: './auth/auth.module#AuthModule'
  },
  {
    // path : '',
    path: 'home',
    loadChildren: './home/home.module#HomeModule',
    canActivate: [AuthGuardService]
  },
  {
    // path : '',
    path: 'vote',
    loadChildren: './vote/vote.module#VoteModule',
    canActivate: [AuthGuardService]
  },
  {
    path: '**',
    redirectTo: 'home'
    //redirectTo: 'dashboard',
    //pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
