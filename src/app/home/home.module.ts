import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HomeComponent } from "./home.component";
import { HomeRoutingModule } from "./home-routing.module";
import { MaterialAppModule } from '../ngmaterial.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { VoteConfirmationComponent } from './vote-confirmation/vote-confirmation.component';

@NgModule({
  declarations: [HomeComponent, VoteConfirmationComponent],
  imports: [
    CommonModule, 
    HomeRoutingModule, 
    MaterialAppModule,
    FormsModule,
    ScrollingModule,
    ReactiveFormsModule],
    entryComponents:[
      VoteConfirmationComponent
    ]
})
export class HomeModule {}
