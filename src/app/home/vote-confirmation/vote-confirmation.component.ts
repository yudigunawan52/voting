import { Component, OnInit, Inject } from '@angular/core';
import { RestApiService } from 'src/app/service/rest-api.service';
import { Router } from '@angular/router';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-vote-confirmation',
  templateUrl: './vote-confirmation.component.html',
  styleUrls: ['./vote-confirmation.component.css']
})
export class VoteConfirmationComponent implements OnInit {
  disabled=false;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<VoteConfirmationComponent>,
    private rest: RestApiService,
    private router: Router) { }

  ngOnInit() {
    console.log(this.data);
    
  }

  yes() {
    this.disabled=true;
    this.rest.postVote({ currentUser: this.data.currentUser, voteUser: this.data.votingUser}).subscribe((data) => {
      if (data["success"]) {
        this.dialogRef.close({status:1});
      }
    });
  }

  no() {
    this.dialogRef.close({status:0});
  }

}
