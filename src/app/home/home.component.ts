import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RestApiService } from '../service/rest-api.service';
import { MatPaginator, MatTableDataSource, MatDialog } from '@angular/material';
import { DataService } from '../service/data.service';
import { VoteConfirmationComponent } from './vote-confirmation/vote-confirmation.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  displayedColumns: string[] = ['Nama', 'Email', 'Action'];
  dataUser: any = [];
  url = this.rest.link_url();
  totData;
  profile;
  currentUser;
  votingUser;
  dataSource = new MatTableDataSource(); 
  @ViewChild(MatPaginator,{static:true}) paginator: MatPaginator;

  constructor(public dialog: MatDialog,
    private http: HttpClient,
    private rest:RestApiService,
    private datas:DataService) {
      datas.getProfile();
     }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  async ngOnInit() {
    await this.rest.getAllUser().subscribe((data) => {
      this.dataUser = data['user'].filter(data => data.email !== this.datas['user']['email']);
      this.totData = this.dataUser.length;
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = this.dataUser;
      this.dataSource.paginator = this.paginator;
    })
          
  }

  vote(arr) {
    this.votingUser = arr[0];
    this.currentUser = arr[1];
    const dialogRef = this.dialog.open(VoteConfirmationComponent, {
      width: '535px',
      height: '262',
      data: { currentUser:this.currentUser,votingUser:this.votingUser }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(result === null);

      (result === null || result === undefined || result === '' || result['status'] === 1)
         ?this.datas['user']['status'] = 1
        : this.datas['user']['status'] = result['status']
    });
  }


}
